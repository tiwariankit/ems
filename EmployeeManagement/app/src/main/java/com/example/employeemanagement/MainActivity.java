package com.example.employeemanagement;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.example.employeemanagement.Activity.HomeScreenActivity;
import com.example.employeemanagement.Activity.LoginActivity;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    Boolean loginstatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences("LoginData", 0);
        loginstatus = sharedPreferences.getBoolean("loginstatus", false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (loginstatus == true) {
                    startActivity(new Intent(MainActivity.this, HomeScreenActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }, 2000);
    }
}
