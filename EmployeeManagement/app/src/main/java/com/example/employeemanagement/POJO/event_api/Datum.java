
package com.example.employeemanagement.POJO.event_api;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("event_id")
    @Expose
    public String eventId;
    @SerializedName("event_title")
    @Expose
    public String eventTitle;
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("image")
    @Expose
    public List<String> image = null;
    @SerializedName("video")
    @Expose
    public List<String> video = null;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;

}