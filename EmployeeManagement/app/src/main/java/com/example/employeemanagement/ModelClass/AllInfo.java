package com.example.employeemanagement.ModelClass;

public class AllInfo {
    private String title, total;
    private int image;

    public AllInfo(String title, String total, int image) {
        this.title = title;
        this.total = total;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public String getTotal() {
        return total;
    }

    public int getImage() {
        return image;
    }
}