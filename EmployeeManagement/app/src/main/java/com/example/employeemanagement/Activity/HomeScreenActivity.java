package com.example.employeemanagement.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.employeemanagement.CustomAdapter.CustomAdapter;
import com.example.employeemanagement.ModelClass.ModelClass;
import com.example.employeemanagement.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeScreenActivity extends AppCompatActivity {

    Context context;
    String api_lname, api_fname, api_image;
    ArrayList<ModelClass> data = new ArrayList<>();
    RecyclerView recyclerView;
    TextView txtUsername;
    CircleImageView profile_image;
    CustomAdapter customAdapter;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        sharedPreferences = getSharedPreferences("LoginData", 0);
        getFindViewById();
        getRecyclerData();
        goProfilePage();
        getUserSharedPreferenceData();
        checkInternetPermission();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.all_info, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.all_Info_menu:
//                startActivity(new Intent(HomeScreenActivity.this, AllInfoActivity.class));
//        }
//        return super.onOptionsItemSelected(item);
//    }

    private void checkInternetPermission() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null) {
            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                Log.d("cc", "WIFI Enabled");
            } else if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                Toast.makeText(this, "Connection Available", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "No Internet Access", Toast.LENGTH_SHORT).show();
        }
    }

    private void getFindViewById() {
        profile_image = findViewById(R.id.profile_image1);
        recyclerView = findViewById(R.id.rv);
        txtUsername = findViewById(R.id.t2);
        progressBar = findViewById(R.id.homeprogress);
    }

    private void getUserSharedPreferenceData() {
        api_fname = sharedPreferences.getString("api_fname", "");
        api_lname = sharedPreferences.getString("api_lname", "");
        api_image = sharedPreferences.getString("api_image", "");
        Picasso.with(getApplicationContext()).load("https://intern.fmv.cc/EmployeeAdmin/assets/uploads/" + api_image).into(profile_image, new Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                progressBar.setVisibility(View.GONE);
                profile_image.setImageResource(R.drawable.user);
            }
        });
        txtUsername.setText(api_fname + "\t" + api_lname);

    }

    private void getRecyclerData() {
        data.add(new ModelClass(R.drawable.attendance, "Attendence"));
        data.add(new ModelClass(R.drawable.complain, "Complain"));
        data.add(new ModelClass(R.drawable.document, "Document"));
        data.add(new ModelClass(R.drawable.event, "Event"));
        data.add(new ModelClass(R.drawable.holiday, "holiday"));
        data.add(new ModelClass(R.drawable.leave, "Leave"));
        data.add(new ModelClass(R.drawable.connectivity, "Connect"));
        data.add(new ModelClass(R.drawable.document, "Poll"));
        data.add(new ModelClass(R.drawable.event, "All Info"));
        customAdapter = new CustomAdapter(data);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        recyclerView.setAdapter(customAdapter);
    }

    private void goProfilePage() {
        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeScreenActivity.this, ProfilePageActivity.class));
                new Intent().setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserSharedPreferenceData();
    }
}
