package com.example.employeemanagement.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.employeemanagement.POJO.login_api.Login;
import com.example.employeemanagement.R;
import com.example.employeemanagement.Retrofit.ApiInteface;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {


    String email, password, pp, email_pattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String api_contact,api_email, api_Fname, api_Lname, api_street, api_gender, api_image, api_birthdate, api_joindate, api_department_name, api_designation_name, api_role_id, api_area, api_country, api_city, api_state;
    int api_emp_id;
    private TextView forgot_password;
    private Button login_btn;
    EditText editText_email, editText_password;
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        progressDialog = new ProgressDialog(this);
        sharedPreferences = getSharedPreferences("LoginData", 0);
        editor = sharedPreferences.edit();

        getFindViewBYId();
        goForgotActivity();
        goHomeScreen();
    }

    private void getFindViewBYId() {
        forgot_password = findViewById(R.id.forgot_password);
        login_btn = findViewById(R.id.btn_login);
        editText_email = findViewById(R.id.email);
        editText_password = findViewById(R.id.pass);
    }

    private void editTextValidation() {

        if (editText_email.getText().toString().isEmpty() || editText_password.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please fill out the field", Toast.LENGTH_SHORT).show();
        } else if (!editText_email.getText().toString().matches(email_pattern)) {
            Toast.makeText(this, "Please enter valid email", Toast.LENGTH_SHORT).show();
        } else {
            getRetrofitData();
        }
    }

    private void goHomeScreen() {
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  getRetrofitData();
                editTextValidation();

            }
        });
    }

    protected void goForgotActivity() {
        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
                new Intent().setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
        });
    }

    private void getRetrofitData() {
        email = editText_email.getText().toString();
        password = editText_password.getText().toString();
        pp = getMd5(password);
        // editTextValidation();
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<Login> call = apiInteface.getLogin(email, password);
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (response.isSuccessful()) {
//                    Log.d("res",response.body().getStatus().toString());
                    if (response.body().getStatus() == false) {
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, "please enter valid email and password", Toast.LENGTH_SHORT).show();
                    } else {
                        api_emp_id = Integer.parseInt(response.body().getData().get(0).getEmployeeId());
                        api_Fname = response.body().getData().get(0).getFname();
                        api_Lname = response.body().getData().get(0).getLname();
                        api_email = response.body().getData().get(0).getEmail();
                        api_street = response.body().getData().get(0).getStreet();
                        Log.d("an",api_Fname);
                        api_contact = response.body().getData().get(0).getContact();
                        api_gender = response.body().getData().get(0).getGender();
                        api_image = response.body().getData().get(0).getImage();
                        api_state = response.body().getData().get(0).getState();
                        api_joindate = response.body().getData().get(0).getJoindate();
                        api_birthdate = response.body().getData().get(0).getBirthdate();
                        api_department_name = response.body().getData().get(0).getDepartment_name();
                        api_designation_name = response.body().getData().get(0).getDesignation_name();
                        api_role_id = response.body().getData().get(0).getRoleId();
                        api_street = response.body().getData().get(0).getStreet();
                        api_area = response.body().getData().get(0).getArea();
                        api_city = response.body().getData().get(0).getCity();
                        api_country = response.body().getData().get(0).getCountry();

                        editor.putInt("api_emp_id", api_emp_id);
                        editor.putString("api_fname", api_Fname);
                        editor.putString("api_lname", api_Lname);
                        editor.putString("api_image", api_image);
                        editor.putString("api_email", api_email);
                        editor.putString("api_contact", api_contact);
                        editor.putString("api_street", api_street);
                        editor.putString("api_gender", api_gender);
                        editor.putString("api_state", api_state);
                        editor.putString("api_joindate", api_joindate);
                        editor.putString("api_birthdate", api_birthdate);
                        editor.putString("api_area", api_area);
                        editor.putString("api_city", api_city);
                        editor.putString("api_country", api_country);
                        editor.putString("api_role_id", api_role_id);
                        editor.putString("api_department_name", api_department_name);
                        editor.putString("api_designation_name", api_designation_name);
                        editor.putBoolean("loginstatus", true);
                        editor.apply();
                        progressDialog.dismiss();
                        startActivity(new Intent(LoginActivity.this, HomeScreenActivity.class));
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();

//                progressDialog.dismiss();
            }
        });
    }

    public static String getMd5(String input) {
        try {
            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());
            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);
            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
