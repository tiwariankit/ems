package com.example.employeemanagement.CustomAdapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.employeemanagement.Activity.EventDetailActivity;
import com.example.employeemanagement.POJO.event_api.Datum;
import com.example.employeemanagement.R;

import java.util.ArrayList;
import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyEventHolder> {

    Context context;
    List<Datum> list;
    String main_image, img1,img2,description;
    List<String> allImages=new ArrayList<>();

    public EventAdapter(Context context, List<Datum> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyEventHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_data, parent, false);
        return new MyEventHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyEventHolder holder, final int position) {
        main_image = list.get(position).image.get(0);
        Glide.with(holder.itemView.getContext()).load("https://intern.fmv.cc/EmployeeAdmin/assets/uploads/" + main_image).into(holder.event_image);
        // Log.d("a",String.valueOf("http://192.168.2.5/EmployeeAdmin/assets/uploads/"+list.get(position).getImage()));
        holder.event_name.setText(list.get(position).eventTitle);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context = holder.itemView.getContext();
                for (int i=0;i<list.get(position).image.size();i++){
                    allImages=list.get(position).image;
                    Log.d("s", String.valueOf(allImages.size()));
                }
                description=list.get(position).description;
                img1=list.get(0).image.get(1);
                Intent intent = new Intent(context, EventDetailActivity.class);
                intent.putStringArrayListExtra("image_list", (ArrayList<String>) allImages);
               //intent.putExtra("event_image2", img2);
                intent.putExtra("event_name", description);//
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyEventHolder extends RecyclerView.ViewHolder {
        ImageView event_image;
        TextView event_name;

        public MyEventHolder(@NonNull View itemView) {
            super(itemView);
            event_image = itemView.findViewById(R.id.event_image);
            event_name = itemView.findViewById(R.id.event_name);
        }
    }
}
