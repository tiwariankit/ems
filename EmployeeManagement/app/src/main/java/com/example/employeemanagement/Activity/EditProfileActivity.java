package com.example.employeemanagement.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.TextView;

import com.example.employeemanagement.R;

public class EditProfileActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView titleTextView;
    TextView firstname,lastname,email,phone,designation,department;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        getFindViewById();
        getToolBar();
    }

    private void getFindViewById(){
        firstname=findViewById(R.id.firstname_txt);
        lastname=findViewById(R.id.lastname_txt);
        email=findViewById(R.id.email_txt);
        //phone=findViewById(R.id.phone_txt);
        designation=findViewById(R.id.department_txt);
        department=findViewById(R.id.department_txt);
    }
    private void getToolBar() {
        toolbar=findViewById(R.id.toolbars);
        titleTextView=findViewById(R.id.title_text);
        titleTextView.setText("Document");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Edit Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
