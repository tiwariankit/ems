package com.example.employeemanagement.Activity;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.employeemanagement.POJO.document_type_api.Datum;
import com.example.employeemanagement.POJO.document_type_api.GetDocuments;
import com.example.employeemanagement.POJO.document_type_api.InsertDocument;
import com.example.employeemanagement.R;
import com.example.employeemanagement.Retrofit.ApiInteface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DocumentActivity extends AppCompatActivity {

    List<String> documentType;
    Toolbar toolbar;
    TextView titleTextView;
    Spinner document_type_spinner;
    Button btn_document;
    Boolean selected = true;
    int document_id;
    int api_emp_id;
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document);
        sharedPreferences = getSharedPreferences("LoginData", 0);
        api_emp_id = sharedPreferences.getInt("api_emp_id", 0);
        progressDialog = new ProgressDialog(this);
        documentType = new ArrayList<>();
        documentType.add(0, "Please select Document");
        getFindViewById();
        getRetrofitData();
        getToolBar();
        getSpinnerValueName();
        applyForDocument();
    }

    private void getFindViewById() {
        document_type_spinner = findViewById(R.id.document_type_spinner);
        btn_document = findViewById(R.id.btn_document);
    }

    private void getToolBar() {
        toolbar = findViewById(R.id.toolbars);
        titleTextView = findViewById(R.id.title_text);
        titleTextView.setText("Document");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(titleTextView.getText().toString());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void getRetrofitData() {

        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetDocuments> call = apiInteface.getDocumentType();
        call.enqueue(new Callback<GetDocuments>() {
            @Override
            public void onResponse(Call<GetDocuments> call, Response<GetDocuments> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == false) {
                        progressDialog.dismiss();
                    } else {
                        List<Datum> data = response.body().getData();
                        for (int i = 0; i < data.size(); i++) {
                            documentType.add(data.get(i).getDocumentType());
                        }
                        progressDialog.dismiss();
                        ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, documentType) {
                            @Override
                            public boolean isEnabled(int position) {
//                                return super.isEnabled(position);
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        };
                        document_type_spinner.setAdapter(arrayAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetDocuments> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(DocumentActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getSpinnerValueName() {
        document_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (selected) {
                    selected = false;
                } else {
                    document_id = position;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void applyForDocument() {
        btn_document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (document_type_spinner.getSelectedItem() == "Please select Document") {
                    Toast.makeText(DocumentActivity.this, "Please select Document", Toast.LENGTH_SHORT).show();
                } else {
                    ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
                    Call<InsertDocument> insertDocumentCall = apiInteface.insertDocument(api_emp_id, document_id);
                    insertDocumentCall.enqueue(new Callback<InsertDocument>() {
                        @Override
                        public void onResponse(Call<InsertDocument> call, Response<InsertDocument> response) {
//                            Log.d("res", response.body().getStatus().toString());
                            if (response.isSuccessful()) {
                                if (response.body().getStatus() == true) {
                                    Toast.makeText(DocumentActivity.this, "Request Applied Successfully", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<InsertDocument> call, Throwable t) {
                            Toast.makeText(DocumentActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

    }
}