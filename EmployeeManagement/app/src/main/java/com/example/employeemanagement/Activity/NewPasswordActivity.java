package com.example.employeemanagement.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.employeemanagement.R;

public class NewPasswordActivity extends AppCompatActivity {

   Toolbar toolbar;
   TextView titleTextView;
   private Button new_pass_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);
        new_pass_btn=findViewById(R.id.new_pass_btn);
        pressNewPasswordButton();
        getToolBar();
    }
    private void getToolBar(){
        toolbar = findViewById(R.id.toolbars);
        titleTextView=findViewById(R.id.title_text);
        titleTextView.setText("Reset Password");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(titleTextView.getText().toString());
    }
    private void pressNewPasswordButton() {
        new_pass_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewPasswordActivity.this,LoginActivity.class));
            }
        });
    }
}
