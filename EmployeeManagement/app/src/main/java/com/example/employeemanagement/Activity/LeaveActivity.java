package com.example.employeemanagement.Activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.employeemanagement.POJO.leave_type_api.Datum;
import com.example.employeemanagement.POJO.leave_type_api.GetLeaveType;
import com.example.employeemanagement.POJO.leave_type_api.InsertLeave;
import com.example.employeemanagement.R;
import com.example.employeemanagement.Retrofit.ApiInteface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeaveActivity extends AppCompatActivity {
    EditText from_date_edit_text, to_date_edit_text;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    String FromdateStringFormat, TodateStringFormat, spinnerDataName;
    int leave_type_id, leave_type_id1, api_emp_id;
    ProgressDialog progressDialog;
    List<String> leaveType = new ArrayList<>();
    List<Integer> leaveTypeId = new ArrayList<>();
    List<Integer> totaldays = new ArrayList<>();
    List<Datum> data;
    private int months, years, day;
    DatePickerDialog datePickerDialog;
    DatePickerDialog datePickerDialog2;
    Calendar calendar;
    EditText reason_edt;
    Button submit;
    Date from_day, to_day;
    String from = "", to = "";
    Toolbar toolbar;
    Spinner leave_type_spinner;
    TextView total_days, titleTextView;
    Long from_date_value;
    Boolean selected = true;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave);
        progressDialog = new ProgressDialog(this);
        sharedPreferences = getSharedPreferences("LoginData", 0);
        allMethod();

        leaveType.add(0, "Please Select Leave");
        calendar = Calendar.getInstance();
        years = calendar.get(Calendar.YEAR);
        months = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        from_date_edit_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerDialog = new DatePickerDialog(LeaveActivity.this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//                        time variable return the selected date and time
                        FromdateStringFormat = dateFormat.format(calendar.getTime());
                        from_day = calendar.getTime();
                        Log.d("ff0", FromdateStringFormat);
                        from_date_value = calendar.getTimeInMillis();
//                        Log.d("time", String.valueOf(from_day.getTime()));
                        if (to_date_edit_text.getText().toString().equals("")) {
                            from_date_edit_text.setText(FromdateStringFormat);

                        } else {
                            // select from date
                            from = dayOfMonth + "-" + (month + 1) + "-" + year;
//                            Log.d("black", from);
//                            Log.d("hello", to);
                            if (from.equals(to)) {
                                from_date_edit_text.setText(from);
                                total_days.setText(String.valueOf(Math.abs((from_day.getTime() - to_day.getTime()) / (24 * 60 * 60 * 1000)) + 1));
                            } else if (from_day.after(to_day)) {
                                from_date_edit_text.setText("");
                                to_date_edit_text.setText("");
                                total_days.setText("");
                                Toast.makeText(LeaveActivity.this, "Select Valid Date", Toast.LENGTH_SHORT).show();

                            }
                            if (from_day.before(to_day)) {
                                from_date_edit_text.setText(from);
                                to_date_edit_text.setText(to);
                                Log.d("abc", String.valueOf(Math.abs((from_day.getTime() - to_day.getTime()) / (24 * 60 * 60 * 1000)) + 1) + "\n=" + to + "\n=" + from);
                                Log.d("one", String.valueOf(from_day.getTime()));
                                Log.d("two", String.valueOf(calendar.getTime()));
                                total_days.setText(String.valueOf(Math.abs((from_day.getTime() - to_day.getTime()) / (24 * 60 * 60 * 1000)) + 2));
                            }
                        }

                    }
                }, years, months, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        to_date_edit_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (from_date_edit_text.getText().toString().equals("")) {
                    Toast.makeText(LeaveActivity.this, "please select from date", Toast.LENGTH_SHORT).show();
                } else {
                    datePickerDialog2 = new DatePickerDialog(LeaveActivity.this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            calendar = Calendar.getInstance();
                            calendar.set(Calendar.YEAR, year);
                            calendar.set(Calendar.MONTH, month);
                            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            TodateStringFormat = dateFormat.format(calendar.getTime());
                            to_day = calendar.getTime();

//                        Log.d("timess", String.valueOf(to_day.getTime()));
//                        Log.d("times", String.valueOf(Math.abs((from_day.getTime() - to_day.getTime()) / (24 * 60 * 60 * 1000)) + 1));

                            if (to_day.before(from_day)) {
                                to_date_edit_text.setText("");
                                total_days.setText("");
                                Toast.makeText(LeaveActivity.this, "Not Valid", Toast.LENGTH_SHORT).show();
//                            reason.setText("");

                            } else {
                                total_days.setText(String.valueOf(Math.abs((from_day.getTime() - to_day.getTime()) / (24 * 60 * 60 * 1000)) + 1));
                                to_date_edit_text.setText(TodateStringFormat);
                                to = dayOfMonth + "-" + (month + 1) + "-" + year;
                            }
                        }
                    }, years, months, day);
                    datePickerDialog2.getDatePicker().setMinDate(from_date_value - 1000);
                    datePickerDialog2.show();
                }

            }
        });
//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
    }

    private void getToolBar() {
        toolbar = findViewById(R.id.toolbars);
        titleTextView = findViewById(R.id.title_text);
        titleTextView.setText("Leave");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(titleTextView.getText().toString());
    }

    private void getFindViewById() {
        from_date_edit_text = findViewById(R.id.from_date_edittext);
        to_date_edit_text = findViewById(R.id.to_date_edittext);
        reason_edt = findViewById(R.id.reason_edittext);
        total_days = findViewById(R.id.total_days);
        submit = findViewById(R.id.submit);
        leave_type_spinner = findViewById(R.id.leave_type_spinner);
    }

    private void getRetrofitData() {

        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();//

        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetLeaveType> call = apiInteface.getLevaveType();
        call.enqueue(new Callback<GetLeaveType>() {
            @Override
            public void onResponse(Call<GetLeaveType> call, Response<GetLeaveType> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == false) {
                        progressDialog.dismiss();
                    } else {
                        data = response.body().getData();
                        Log.d("data", String.valueOf(data.size()));
                        for (int i = 1; i < data.size(); i++) {
                            leaveType.add(data.get(i).getLeaveType());
                            // leaveTypeId.add(data.get(i).getLeaveTypeId());
                            //leave_type_id1 =data.get(i).getLeaveTypeId();
                            Log.d("id", String.valueOf(leave_type_id1));
                        }
                        progressDialog.dismiss();
                        ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, leaveType) {
                            @Override
                            public boolean isEnabled(int position) {
//                                return super.isEnabled(position);
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        };
                        leave_type_spinner.setAdapter(arrayAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetLeaveType> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(LeaveActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getSpinnerValueName() {
        leave_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (selected) {
                    selected = false;
                } else {
                    leave_type_id = position;
                    //Toast.makeText(LeaveActivity.this, ""+leave_type_id1, Toast.LENGTH_SHORT).show();
                    spinnerDataName = leaveType.get(position);
                    leave_type_id1 = data.get(position).getLeaveTypeId();
                    Log.d("aa", String.valueOf(leave_type_id1));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void applyLeave() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (leave_type_spinner.getSelectedItem() == "Please Select Leave") {
                    Toast.makeText(LeaveActivity.this, "Please Select Leave", Toast.LENGTH_SHORT).show();
                } else if (from_date_edit_text.getText().toString().trim().length() == 0) {
                    Toast.makeText(LeaveActivity.this, "Please select From Date", Toast.LENGTH_SHORT).show();
                } else if (to_date_edit_text.getText().toString().trim().length() == 0) {
                    Toast.makeText(LeaveActivity.this, "Plase select To Date", Toast.LENGTH_SHORT).show();
                } else if (reason_edt.getText().toString().trim().length() == 0) {
                    Toast.makeText(LeaveActivity.this, "Please enter reason", Toast.LENGTH_SHORT).show();
                } else {
                    api_emp_id = sharedPreferences.getInt("api_emp_id", 0);
                    insertLeave();
                    totaldays.add(Integer.parseInt(total_days.getText().toString()));
                }
            }
        });
    }

    private void allMethod() {
        getFindViewById();
        getToolBar();
        getRetrofitData();
        getSpinnerValueName();
        applyLeave();
    }

    private void insertLeave() {
        progressDialog.setMessage("Please Wait");
        progressDialog.show();
        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<InsertLeave> insertLeaveCall = apiInteface.insertLeave(api_emp_id, leave_type_id1, reason_edt.getText().toString(), FromdateStringFormat, TodateStringFormat);
        insertLeaveCall.enqueue(new Callback<InsertLeave>() {
            @Override
            public void onResponse(Call<InsertLeave> call, Response<InsertLeave> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == true) {
                        from_date_edit_text.setText("");
                        to_date_edit_text.setText("");
                        reason_edt.setText("");
                        progressDialog.dismiss();
                        Toast.makeText(LeaveActivity.this, "Leave Apply Successfylly", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getStatus() == false) {
                        progressDialog.dismiss();
                        Toast.makeText(LeaveActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<InsertLeave> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(LeaveActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
