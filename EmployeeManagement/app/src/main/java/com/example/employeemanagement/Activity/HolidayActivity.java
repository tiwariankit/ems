package com.example.employeemanagement.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.employeemanagement.CustomAdapter.HolidayAdapter;
import com.example.employeemanagement.ModelClass.HolidayModel;
import com.example.employeemanagement.POJO.holiday_api.Datum;
import com.example.employeemanagement.POJO.holiday_api.GetHoliday;
import com.example.employeemanagement.R;
import com.example.employeemanagement.Retrofit.ApiInteface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HolidayActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    Toolbar toolbar;
    TextView titleTextView;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    List<Datum> addHoliday = new ArrayList<>();
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holiday);
        sharedPreferences = getSharedPreferences("LoginData", 0);
        editor=sharedPreferences.edit();
        progressDialog = new ProgressDialog(this);
        getFindViewById();
        getToolBar();
        getRecyclerViewData();
    }

    private void getFindViewById() {
        recyclerView = findViewById(R.id.holidayRecycler);
    }

    private void getToolBar() {
        toolbar = findViewById(R.id.toolbars);
        titleTextView = findViewById(R.id.title_text);
        titleTextView.setText("Holiday");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(titleTextView.getText().toString());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void getRecyclerViewData() {
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetHoliday> getHolidayCall = apiInteface.getHolidayList();
        getHolidayCall.enqueue(new Callback<GetHoliday>() {
            @Override
            public void onResponse(Call<GetHoliday> call, Response<GetHoliday> response) {
//                Log.d("res",response.body().getStatus().toString());
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == true) {
                        addHoliday = response.body().getData();
                        progressDialog.dismiss();
                        HolidayAdapter holidayAdapter = new HolidayAdapter(addHoliday, getApplicationContext());
                        recyclerView.setAdapter(holidayAdapter);

                    }
                }
            }

            @Override
            public void onFailure(Call<GetHoliday> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(HolidayActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
