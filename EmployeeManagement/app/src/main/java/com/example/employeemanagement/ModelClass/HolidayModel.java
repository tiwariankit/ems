package com.example.employeemanagement.ModelClass;

public class HolidayModel {
    String date,month,holiday;

    public HolidayModel(String date, String month, String holiday) {
        this.date = date;
        this.month = month;
        this.holiday = holiday;
    }

    public String getDate() {
        return date;
    }

    public String getMonth() {
        return month;
    }

    public String getHoliday() {
        return holiday;
    }
}
