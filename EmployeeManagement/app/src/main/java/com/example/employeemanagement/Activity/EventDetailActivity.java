package com.example.employeemanagement.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.employeemanagement.CustomAdapter.EventDetailAdapter;
import com.example.employeemanagement.ModelClass.EventDetailModel;
import com.example.employeemanagement.R;

import java.util.ArrayList;
import java.util.List;

public class EventDetailActivity extends AppCompatActivity {

    ArrayList<EventDetailModel> eventDetail = new ArrayList<>();
    ViewPager viewPager;
    Toolbar toolbar;
    TextView titleTextView;
   // WebView webView;
    TextView event_detail_description;
    String event_name;

    List<String> allImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        getFindViewById();
        getIntentData();
        addDataInViewPager();
        getToolBar();
    }

    private void getFindViewById() {
        viewPager = findViewById(R.id.viewpager);
        event_detail_description = findViewById(R.id.event_detail_discription);
     //   webView=findViewById(R.id.webView);
    }
    private void getToolBar(){
        toolbar = findViewById(R.id.toolbars);
        titleTextView=findViewById(R.id.title_text);
        titleTextView.setText("Events");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(titleTextView.getText().toString());
    }

    private void getIntentData() {
        allImages = getIntent().getStringArrayListExtra("image_list");
//        Log.d("e1", String.valueOf(event_image1));
      //  event_image2 = getIntent().getIntExtra("event_image2",0);
        event_name = getIntent().getStringExtra("event_name");
        event_detail_description.setText(Html.fromHtml(event_name), TextView.BufferType.SPANNABLE);
//        webView.loadData( event_name,"text/html", "UTF-8");
    }

    private void addDataInViewPager() {
        eventDetail.add(new EventDetailModel(allImages));
        EventDetailAdapter eventDetailAdapter = new EventDetailAdapter(allImages, getApplicationContext());
        viewPager.setAdapter(eventDetailAdapter);
        viewPager.setOffscreenPageLimit(allImages.size());
    }
}
