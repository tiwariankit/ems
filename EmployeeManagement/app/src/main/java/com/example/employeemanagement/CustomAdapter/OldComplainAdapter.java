package com.example.employeemanagement.CustomAdapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.employeemanagement.Activity.ComplainActivity;
import com.example.employeemanagement.Activity.ComplainDetailsActivity;
import com.example.employeemanagement.POJO.old_complain_api.Datum;
import com.example.employeemanagement.POJO.old_complain_api.GetOldComplain;
import com.example.employeemanagement.R;

import java.util.List;

public class OldComplainAdapter extends RecyclerView.Adapter<OldComplainAdapter.MyOldComplainHolder> {
    List<Datum> getOldComplainList;
    Context context;
    String dateFormat;

    public OldComplainAdapter(List<Datum> getOldComplainList, Context context) {
        this.getOldComplainList = getOldComplainList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyOldComplainHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.oldcomplaindata, parent, false);
        return new MyOldComplainHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyOldComplainHolder holder, int position) {
        holder.complain_type.setText(getOldComplainList.get(position).getComplainType());
        dateFormat = getOldComplainList.get(position).getDate();
        holder.complain_date.setText(dateFormat.substring(0, 10));
        // holder.complain_reason.setText(getOldComplainList.get(position).getDescription());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context = holder.itemView.getContext();
                //Toast.makeText(context, getOldComplainList.get(position).getDescription(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, ComplainDetailsActivity.class);
                intent.putExtra("complain_type", getOldComplainList.get(position).getComplainType());
                intent.putExtra("complain_date", getOldComplainList.get(position).getDate());
                intent.putExtra("complain_discription", getOldComplainList.get(position).getDescription());
                context.startActivity(intent);
                Log.d("desc", getOldComplainList.get(position).getDescription());
            }
        });

    }

    @Override
    public int getItemCount() {
        return getOldComplainList.size();
    }

    public class MyOldComplainHolder extends RecyclerView.ViewHolder {

        TextView complain_type, complain_date, complain_reason;

        public MyOldComplainHolder(@NonNull View itemView) {
            super(itemView);
            complain_type = itemView.findViewById(R.id.complain_type);
            complain_date = itemView.findViewById(R.id.complain_date);
            // complain_reason = itemView.findViewById(R.id.complain_reason);
        }
    }
}
