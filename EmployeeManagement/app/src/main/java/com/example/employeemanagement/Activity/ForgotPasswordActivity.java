package com.example.employeemanagement.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.employeemanagement.R;

public class ForgotPasswordActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView titleTextView;
    private Button send_otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        send_otp = findViewById(R.id.send_otp_btn);

        sendForgotPassword();
        getToolBar();
    }

    private void getToolBar() {
        toolbar = findViewById(R.id.toolbars);
        titleTextView=findViewById(R.id.title_text);
        titleTextView.setText("Events");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Forgot Password");
    }

    private void sendForgotPassword() {
        send_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ForgotPasswordActivity.this, VerificationForgotActivity.class));
                finish();
            }
        });

    }
}
