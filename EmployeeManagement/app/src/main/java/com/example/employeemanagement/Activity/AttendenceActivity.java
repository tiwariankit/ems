package com.example.employeemanagement.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.example.employeemanagement.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AttendenceActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView titleTextView;
    CalendarView calendarView;
    CoordinatorLayout coordinatorLayout;
    BottomSheetBehavior bottomSheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendence);
        getToolBar();
        getFindViewById();
        getCalenderView();
        setBottomSheet();
        setMinMaxDateInCalenderView();
    }

    private void getFindViewById() {
        calendarView = findViewById(R.id.calenderView);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
    }

    private void getToolBar() {
        toolbar = findViewById(R.id.toolbars);
        titleTextView = findViewById(R.id.title_text);
        titleTextView.setText("Attendence");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(titleTextView.getText().toString());
    }

    private void setMinMaxDateInCalenderView() {
        Calendar min = Calendar.getInstance();
        min.set(min.get(Calendar.YEAR), Calendar.JANUARY, 00);
        calendarView.setMinimumDate(min);
        Calendar max = Calendar.getInstance();
        max.get(Calendar.DAY_OF_MONTH);
        calendarView.setMaximumDate(max);
    }

    private void getCalenderView() {
        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });
        List<EventDay> eventDays = new ArrayList<>();
        eventDays.add(new EventDay(Calendar.getInstance(), R.drawable.spash_image));
        calendarView.setEvents(eventDays);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 20);
        calendar.set(Calendar.MONTH, 0);
        eventDays.add(new EventDay(calendar, R.drawable.spash_image));
    }

    private void setBottomSheet() {
        View bottomSheet = findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
    }
}
