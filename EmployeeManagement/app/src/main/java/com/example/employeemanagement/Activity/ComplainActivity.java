package com.example.employeemanagement.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.employeemanagement.POJO.complain_type_api.Datum;
import com.example.employeemanagement.POJO.complain_type_api.GetComplain;
import com.example.employeemanagement.POJO.complain_type_api.InsertComplain;
import com.example.employeemanagement.R;
import com.example.employeemanagement.Retrofit.ApiInteface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComplainActivity extends AppCompatActivity {
    List<String> complainType = new ArrayList<>();
    Boolean selected = true;
    int api_emp_id;
    int complain_id;
    ProgressDialog progressDialog;
    Spinner complain_type_spinner;
    EditText complain_edittext;
    Button btn_complain;
    TextView titleTextView;
    Toolbar toolbar;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complain);
        progressDialog = new ProgressDialog(this);
        sharedPreferences = getSharedPreferences("LoginData", 0);
        api_emp_id = sharedPreferences.getInt("api_emp_id", 0);
        complainType.add(0, "Please Select Complain");
        getFindViewById();
        getRetrofitData();
        getToolBar();
        getSpinnerValueName();
        insertComplain();
    }

    private void getFindViewById() {
        complain_type_spinner = findViewById(R.id.complain_type_spinner);
        complain_edittext = findViewById(R.id.complain_edittext);
        btn_complain = findViewById(R.id.btn_complain);
    }

    private void getToolBar() {
        toolbar = findViewById(R.id.toolbars);
        titleTextView = findViewById(R.id.title_text);
        titleTextView.setText("Complain");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(titleTextView.getText().toString());
    }

    private void getRetrofitData() {

        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetComplain> call = apiInteface.getComplainType();
        call.enqueue(new Callback<GetComplain>() {
            @Override
            public void onResponse(Call<GetComplain> call, Response<GetComplain> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == false) {
                        progressDialog.dismiss();
                    } else {
                        List<Datum> data = response.body().getData();
                        for (int i = 0; i < data.size(); i++) {
                            complainType.add(data.get(i).getComplainType());
                        }
                        progressDialog.dismiss();
                        ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, complainType) {
                            @Override
                            public boolean isEnabled(int position) {
//                                return super.isEnabled(position);
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        };
                        complain_type_spinner.setAdapter(arrayAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetComplain> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ComplainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void insertComplain() {
        btn_complain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (complain_type_spinner.getSelectedItem().toString() == "Please Select Complain") {
                    Toast.makeText(ComplainActivity.this, "Please select Complain Type", Toast.LENGTH_SHORT).show();
                } else if (complain_edittext.getText().toString().trim().length()==0) {
                    Toast.makeText(ComplainActivity.this, "Please enter complain", Toast.LENGTH_SHORT).show();
                } else {
                    progressDialog.setMessage("Please wait...");
                    progressDialog.show();
                    ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
                    Call<InsertComplain> insertComplainCall = apiInteface.insertComplain(api_emp_id, complain_id, complain_edittext.getText().toString());
                    insertComplainCall.enqueue(new Callback<InsertComplain>() {
                        @Override
                        public void onResponse(Call<InsertComplain> call, Response<InsertComplain> response) {
                            if (response.isSuccessful()) {
                                if (response.body().getStatus() == true) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ComplainActivity.this, "Complain Inserted Successfully", Toast.LENGTH_SHORT).show();
                                    complain_edittext.setText("");
                                    startActivity(new Intent(ComplainActivity.this,HomeScreenActivity.class));
                                    finishAffinity();

                                } else if (response.body().getStatus() == false) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ComplainActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<InsertComplain> call, Throwable t) {
                            Toast.makeText(ComplainActivity.this, "No Internet Access", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

    }

    private void getSpinnerValueName() {
        complain_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (selected) {
                    selected = false;
                } else {
                    complain_id = position;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
