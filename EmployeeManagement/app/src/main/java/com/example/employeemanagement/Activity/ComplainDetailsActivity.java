package com.example.employeemanagement.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.TextView;

import com.example.employeemanagement.R;

public class ComplainDetailsActivity extends AppCompatActivity {

    TextView complain_type, complain_date, complain_discription, titleTextView;
   Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complain_details);
        getToolBar();

        complain_type = findViewById(R.id.complain_type);
        complain_date = findViewById(R.id.complain_date);
        complain_discription = findViewById(R.id.complain_discription);

        complain_type.setText(getIntent().getStringExtra("complain_type"));
        complain_date.setText(getIntent().getStringExtra("complain_date"));
        complain_discription.setText(getIntent().getStringExtra("complain_discription"));
    }

    private void getToolBar() {
        toolbar = findViewById(R.id.toolbars);
        titleTextView = findViewById(R.id.title_text);
        titleTextView.setText("Complain Details");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(titleTextView.getText().toString());
    }
}
