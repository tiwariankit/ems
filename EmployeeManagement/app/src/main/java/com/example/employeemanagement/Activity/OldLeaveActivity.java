package com.example.employeemanagement.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.employeemanagement.CustomAdapter.OldLeaveAdapter;
import com.example.employeemanagement.ModelClass.OldLeaveModel;
import com.example.employeemanagement.POJO.old_complain_api.GetOldComplain;
import com.example.employeemanagement.POJO.old_leave_api.Datum;
import com.example.employeemanagement.POJO.old_leave_api.GetOldLeave;
import com.example.employeemanagement.R;
import com.example.employeemanagement.Retrofit.ApiInteface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OldLeaveActivity extends AppCompatActivity {

    ArrayList<OldLeaveModel> oldLeave = new ArrayList<>();
    OldLeaveAdapter oldLeaveAdapter;
    RecyclerView recyclerView;
    TextView titleTextView;
    Toolbar toolbar;
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    int api_emp_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old_leave);
        progressDialog = new ProgressDialog(this);
        sharedPreferences = getSharedPreferences("LoginData", 0);
        editor=sharedPreferences.edit();
        api_emp_id = sharedPreferences.getInt("api_emp_id", 0);
        getFindViewById();
        getRecyclerViewData();
        getToolBar();
    }

    private void getFindViewById() {
        recyclerView = findViewById(R.id.oldLeaveRecycler);
    }

    private void getToolBar() {
        toolbar = findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);
        titleTextView = findViewById(R.id.title_text);
        titleTextView.setText("My Leaves");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(titleTextView.getText().toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.leave, menu);
        MenuItem searchleave = menu.findItem(R.id.search_leave);
        SearchView searchView = (SearchView) searchleave.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setQueryHint("Search your leave");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
//                Toast.makeText(OldLeaveActivity.this, newText, Toast.LENGTH_SHORT).show();
                oldLeaveAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_leave:
                startActivity(new Intent(getApplicationContext(), LeaveActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    private void getRecyclerViewData() {
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetOldLeave> getOldLeaveCall = apiInteface.getOldLeave(api_emp_id);
        getOldLeaveCall.enqueue(new Callback<GetOldLeave>() {
            @Override
            public void onResponse(Call<GetOldLeave> call, Response<GetOldLeave> response) {
                //Log.d("res",response.body().getStatus().toString());
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == true) {
                        List<Datum> data = response.body().getData();
                        Log.i("o", String.valueOf(data.size()));
                        Collections.reverse(data);
                        progressDialog.dismiss();
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        oldLeaveAdapter = new OldLeaveAdapter(data, getApplicationContext());
                        recyclerView.setAdapter(oldLeaveAdapter);
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(OldLeaveActivity.this, "You have not apply any leave", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetOldLeave> call, Throwable t) {
                progressDialog.dismiss();
                //awToast.makeText(OldLeaveActivity.this, "No data available", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void refreshOldLeave() {
        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetOldLeave> getOldLeaveCall = apiInteface.getOldLeave(api_emp_id);
        getOldLeaveCall.enqueue(new Callback<GetOldLeave>() {
            @Override
            public void onResponse(Call<GetOldLeave> call, Response<GetOldLeave> response) {
                //Log.d("res",response.body().getStatus().toString());
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == true) {
                        List<Datum> data = response.body().getData();
                        Collections.reverse(data);
                        progressDialog.dismiss();
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        oldLeaveAdapter = new OldLeaveAdapter(data, getApplicationContext());
                        recyclerView.setAdapter(oldLeaveAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetOldLeave> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(OldLeaveActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        refreshOldLeave();
    }
}
