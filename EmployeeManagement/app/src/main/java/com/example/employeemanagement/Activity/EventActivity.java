package com.example.employeemanagement.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.employeemanagement.CustomAdapter.EventAdapter;
import com.example.employeemanagement.ModelClass.EventModel;
import com.example.employeemanagement.POJO.event_api.Datum;
import com.example.employeemanagement.POJO.event_api.GetEventList;
import com.example.employeemanagement.R;
import com.example.employeemanagement.Retrofit.ApiInteface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    TextView titleTextView;
    Toolbar toolbar;
    ArrayList<EventModel> event = new ArrayList<>();
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    List<Datum> data;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        sharedPreferences= getSharedPreferences("LoginData", 0);
        editor=sharedPreferences.edit();
        progressDialog = new ProgressDialog(this);
        data = new ArrayList<>();
        getFindViewById();
        getRecylerViewData();
        getToolBar();
    }

    private void getFindViewById() {
        recyclerView = findViewById(R.id.eventRecycler);
    }

    private void getToolBar() {
        toolbar = findViewById(R.id.toolbars);
        titleTextView = findViewById(R.id.title_text);
        titleTextView.setText("Events");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(titleTextView.getText().toString());
    }

    private void getRecylerViewData() {

        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetEventList> getEventListCall = apiInteface.getEvent();
        getEventListCall.enqueue(new Callback<GetEventList>() {
            @Override
            public void onResponse(Call<GetEventList> call, Response<GetEventList> response) {
                if (response.isSuccessful()) {
                    if (response.body().status==false) {
                        progressDialog.dismiss();
                        Toast.makeText(EventActivity.this, "No Event Available", Toast.LENGTH_SHORT).show();
                    } else {
                        progressDialog.dismiss();

                            data.addAll(response.body().data);
                            editor.putInt("allEvent",data.size());
                            editor.apply();
                            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                            EventAdapter eventAdapter = new EventAdapter(getApplicationContext(), data);
                            recyclerView.setAdapter(eventAdapter);
                    }
                }
            }
            @Override
            public void onFailure(Call<GetEventList> call, Throwable t) {
            }
        });
    }
}
