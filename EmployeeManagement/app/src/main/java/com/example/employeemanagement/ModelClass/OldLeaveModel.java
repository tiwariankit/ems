package com.example.employeemanagement.ModelClass;

public class OldLeaveModel {
    String leave_style;
    String leave_date;
    String leave_days;
    String leave_staus;

    public OldLeaveModel(String leave_style, String leave_date, String leave_days, String leave_staus) {
        this.leave_style = leave_style;
        this.leave_date = leave_date;
        this.leave_days = leave_days;
        this.leave_staus = leave_staus;
    }

    public String getLeave_style() {
        return leave_style;
    }

    public String getLeave_date() {
        return leave_date;
    }

    public String getLeave_days() {
        return leave_days;
    }

    public String getLeave_staus() {
        return leave_staus;
    }
}
