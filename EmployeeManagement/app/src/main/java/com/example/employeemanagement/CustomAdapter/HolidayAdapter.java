package com.example.employeemanagement.CustomAdapter;

import android.content.Context;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.employeemanagement.ModelClass.HolidayModel;
import com.example.employeemanagement.POJO.holiday_api.Datum;
import com.example.employeemanagement.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class HolidayAdapter extends RecyclerView.Adapter<HolidayAdapter.HolidayViewHolder> {
    List<Datum> addHoliday;
    Context context;
    String dateFormat;
    String month_name;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat outputDateFormat=new SimpleDateFormat("MMMM");

    public HolidayAdapter(List<Datum> addHoliday, Context context) {
        this.addHoliday = addHoliday;
        this.context = context;
    }

    @NonNull
    @Override
    public HolidayViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holiday_data, parent, false);
        return new HolidayViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolidayViewHolder holder, int position) {
        dateFormat = addHoliday.get(position).getDate();
        Log.d("aa", dateFormat);
        try {
            Date date = simpleDateFormat.parse(dateFormat);
            month_name = outputDateFormat.format(date);
            Log.d("dd", month_name);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.holiday_date.setText(dateFormat.substring(8, 10));
        holder.holiday_month.setText(month_name);
        holder.holiday_name.setText(addHoliday.get(position).getHolidayTitle());
    }

    @Override
    public int getItemCount() {
        return addHoliday.size();
    }

    public class HolidayViewHolder extends RecyclerView.ViewHolder {

        TextView holiday_date, holiday_month, holiday_name;

        public HolidayViewHolder(@NonNull View itemView) {
            super(itemView);
            holiday_date = itemView.findViewById(R.id.holiday_date);
            holiday_month = itemView.findViewById(R.id.holiday_month);
            holiday_name = itemView.findViewById(R.id.holiday_name);
        }
    }
}
