package com.example.employeemanagement.CustomAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.employeemanagement.Activity.AllInfoActivity;
import com.example.employeemanagement.Activity.AttendenceActivity;
import com.example.employeemanagement.Activity.ComplainActivity;
import com.example.employeemanagement.Activity.DocumentRequestActivity;
import com.example.employeemanagement.Activity.EventActivity;
import com.example.employeemanagement.Activity.HolidayActivity;
import com.example.employeemanagement.Activity.OldComplainActivity;
import com.example.employeemanagement.Activity.OldLeaveActivity;
import com.example.employeemanagement.Activity.DocumentActivity;
import com.example.employeemanagement.ModelClass.ModelClass;
import com.example.employeemanagement.R;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyHolder> {
    ArrayList<ModelClass> data;
    Context context;

    public CustomAdapter(ArrayList<ModelClass> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customlayout, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyHolder holder, final int position) {
        holder.textView.setText(data.get(position).getName());
        holder.imageView.setImageResource(data.get(position).getImage());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context = holder.itemView.getContext();

                switch (position) {
                    case 0:
                        Intent intent = new Intent(holder.itemView.getContext(), AttendenceActivity.class);
                        context.startActivity(intent);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        break;
                    case 1:
                        Intent intent1 = new Intent(holder.itemView.getContext(), OldComplainActivity.class);
                        context.startActivity(intent1);
                        intent1.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        break;
                    case 2:
                        context.startActivity(new Intent(context, DocumentRequestActivity.class));
                        break;
                    case 3:
                        Intent intent2 = new Intent(holder.itemView.getContext(), EventActivity.class);
                        context.startActivity(intent2);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        break;
                    case 4:
                        Intent intent3 = new Intent(holder.itemView.getContext(), HolidayActivity.class);
                        context.startActivity(intent3);
                        intent3.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        break;
                    case 5:
                        Intent intent4 = new Intent(holder.itemView.getContext(), OldLeaveActivity.class);
                        context.startActivity(intent4);
                        intent4.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        break;
                    case 6:
                        Toast.makeText(context, data.get(position).getName(), Toast.LENGTH_SHORT).show();
                        break;
                    case 7:
                        Toast.makeText(context, data.get(position).getName(), Toast.LENGTH_SHORT).show();
                        break;
                    case 8:
                        Intent intent5=new Intent(holder.itemView.getContext(), AllInfoActivity.class);
                        context.startActivity(intent5);
                        intent5.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        break;
//

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class MyHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        RelativeLayout relativeLayout;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img1);
            textView = itemView.findViewById(R.id.text1);
            relativeLayout = itemView.findViewById(R.id.homeHolder);
        }
    }
}
