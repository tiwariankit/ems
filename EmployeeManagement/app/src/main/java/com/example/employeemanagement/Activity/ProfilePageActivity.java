package com.example.employeemanagement.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.employeemanagement.POJO.ProfileUpdate;
import com.example.employeemanagement.R;
import com.example.employeemanagement.Retrofit.ApiInteface;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilePageActivity extends AppCompatActivity {

    private MenuItem menuHide;
    ProgressDialog progressDialog;
    String api_email, api_fname, api_lname, api_department_name, api_designation_name, api_image, file, file11;
    String files = "https://intern.fmv.cc/EmployeeAdmin/assets/uploads/";
    int api_emp_id;
    String api_contact;
    String phone_edittext_value;
    AlertDialog.Builder alertDailog;
    Boolean loginstatus;
    Toolbar toolbar;
    TextView titleTextView, email_txt, phone_edt, firstname_txt, lastname_txt, designation_txt, department_txt;
    Button btn_edit_profile, btn_logout;
    EditText phone;
    CircleImageView profile_images, edit_profile_image;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public int CAMERA_REQUEST_CODE = 2, RESULT_GALLERY_IMAGE = 3, STORAGE_PERMISSION_CODE = 121;
    Bitmap bitmap;
    File file1;
    RequestBody requestBody;
    MultipartBody.Part part;
    MultipartBody.Part part1;
    String s;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_page);
        progressDialog = new ProgressDialog(this);
        alertDailog = new AlertDialog.Builder(this);
        sharedPreferences = getSharedPreferences("LoginData", 0);
        loginstatus = sharedPreferences.getBoolean("loginstatus", false);
        editor = sharedPreferences.edit();

        getFindViewById();
        getToolBar();
        edit();
        goEditProfileActivity();
        userLogout();
        getUserSharedPreferenceData();
        //requestMultiplePermissions();
    }

    private void getFindViewById() {
        btn_edit_profile = findViewById(R.id.btn_edit_profile);
        btn_logout = findViewById(R.id.btn_logout);
        phone = findViewById(R.id.phone_edt);
        email_txt = findViewById(R.id.email_txt);
        profile_images = findViewById(R.id.profile_images);
        edit_profile_image = findViewById(R.id.edit_profile_images);
        firstname_txt = findViewById(R.id.firstname_txt);
        lastname_txt = findViewById(R.id.lastname_txt);
        department_txt = findViewById(R.id.department_txt);
        designation_txt = findViewById(R.id.designation_txt);
        progressBar = findViewById(R.id.profileprogress);
    }

    private void getUserSharedPreferenceData() {

        api_emp_id = sharedPreferences.getInt("api_emp_id", 0);
        api_contact = sharedPreferences.getString("api_contact", "");
        api_email = sharedPreferences.getString("api_email", "");
        api_image = sharedPreferences.getString("api_image", "");
        api_fname = sharedPreferences.getString("api_fname", "");
        api_lname = sharedPreferences.getString("api_lname", "");
        api_designation_name = sharedPreferences.getString("api_designation_name", "");
        api_department_name = sharedPreferences.getString("api_department_name", "");
        email_txt.setText(api_email);
        firstname_txt.setText(api_fname);
        lastname_txt.setText(api_lname);
        designation_txt.setText(api_designation_name);
        department_txt.setText(api_department_name);
        phone.setText(api_contact);
        Picasso.with(ProfilePageActivity.this).load("https://intern.fmv.cc/EmployeeAdmin/assets/uploads/" + api_image).into(profile_images, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                progressBar.setVisibility(View.GONE);
                profile_images.setImageResource(R.drawable.user);
            }
        });
    }

    private void edit() {
        edit_profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestMultiplePermissions();
            }
        });
    }

    private void selectOption() {
        String[] item = {"Gallery", "Camera"};
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Choose Image From")
                .items(item)
                .itemsCallback((dialog1, itemView, which, text) -> {
                    if (which == 0) {
                        //checkVersionGalary();
                        openGalary();
                    } else if (which == 1) {
                        //checkVersionCamera();
                        openCamera();

                    }
                })
                .build();
        dialog.show();
    }

//    private void checkVersionGalary() {
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
//                openGalary();
//            } else {
//                askStoragePermission();
//            }
//        }
//
//    }

    private void openGalary() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, RESULT_GALLERY_IMAGE);
    }

//    public void askStoragePermission() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
//        }
//    }

    private void getToolBar() {
        toolbar = findViewById(R.id.toolbars);
        titleTextView = findViewById(R.id.title_text);
        titleTextView.setText("Profile");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(titleTextView.getText().toString());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void editPhoneNumber() {
        if (phone.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter contact no", Toast.LENGTH_SHORT).show();
        } else if (phone.getText().toString().length() < 10) {
            Toast.makeText(this, "Please enter valid phone no", Toast.LENGTH_SHORT).show();
        } else {
            phone_edittext_value = phone.getText().toString();
            if (file == null) {
                s = sharedPreferences.getString("api_image", "");
                file1 = new File(s);
                requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), s);
                part1 = MultipartBody.Part.createFormData("files", s, requestBody);
            } else {
                file1 = new File(file);
                requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
                part = MultipartBody.Part.createFormData("files", file1.getName(), requestBody);
            }
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
            progressDialog.setCancelable(false);
            ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
            Call<ProfileUpdate> profileUpdateCall = apiInteface.getProfileUpdate(api_emp_id, phone_edittext_value, part == null ? part = part1 : part);
            profileUpdateCall.enqueue(new Callback<ProfileUpdate>() {
                @Override
                public void onResponse(Call<ProfileUpdate> call, Response<ProfileUpdate> response) {
                    progressBar.setVisibility(View.VISIBLE);
                    if (response.isSuccessful()) {
                        if (response.body().getStatus() == true) {
                            String unique_img = response.body().getPost().getImage();
                            if (unique_img == null) {
                                editor.putString("api_image", file1.getName());
                            } else {
                                editor.putString("api_image", unique_img);
                            }
                            String unique_contact = response.body().getPost().getContact();
                            Log.d("c", unique_contact);
                            editor.putString("api_contact", unique_contact);
                            editor.apply();
                            phone.setText(sharedPreferences.getString("api_contact", ""));
                            Log.d("Ankit", files + sharedPreferences.getString("api_image", ""));
                            Picasso.with(getApplicationContext()).load(files + sharedPreferences.getString("api_image", "")).into(profile_images, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    progressBar.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    progressBar.setVisibility(View.GONE);
                                    profile_images.setImageResource(R.drawable.user);
                                }
                            });
                            phone.setEnabled(false);
                            menuHide.setVisible(false);
                            edit_profile_image.setVisibility(View.INVISIBLE);
                            btn_edit_profile.setVisibility(View.GONE);
                            btn_logout.setVisibility(View.VISIBLE);
                            progressDialog.dismiss();
                            Toast.makeText(ProfilePageActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    Log.d("res", response.body().getStatus().toString());
                }

                @Override
                public void onFailure(Call<ProfileUpdate> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(ProfilePageActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_profile_menu, menu);
        menuHide = menu.findItem(R.id.edit_profile_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_profile_menu:
                menuHide.setVisible(false);
                edit_profile_image.setVisibility(View.VISIBLE);
                enableDisableEditText();
                enableDisableButton();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void enableDisableEditText() {
        if (phone.isEnabled()) {
            phone.setEnabled(false);
        } else {
            phone.setEnabled(true);
            phone.setSelection(phone.getText().length());
        }
    }

    @SuppressLint("ResourceAsColor")
    private void enableDisableButton() {
        if (btn_logout.isEnabled()) {
            btn_edit_profile.setText("Update  Profile");
            btn_edit_profile.setEnabled(true);
        } else {
            btn_logout.setEnabled(false);
            btn_logout.setVisibility(View.GONE);
//            btn_edit_profile.setTextColor(Color.WHITE);
        }
    }

    private void goEditProfileActivity() {
        btn_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPhoneNumber();
            }
        });
    }

    private void userLogout() {
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDailog.setTitle("Logout");
                alertDailog.setMessage("Are you sure you want to logout?");
                alertDailog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editor.clear();
                        editor.apply();
                        finishAffinity();
                        startActivity(new Intent(ProfilePageActivity.this, LoginActivity.class));
                        Toast.makeText(ProfilePageActivity.this, "Logout Successfully", Toast.LENGTH_LONG).show();
                    }
                });
                alertDailog.setNegativeButton(android.R.string.no, null);
                alertDailog.show();
            }
        });

    }


//    private void checkVersionCamera() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
//                openCamera();
//            } else {
//                askPermission();
//            }
//        }
//    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_REQUEST_CODE);
    }

//    private void askPermission() {
//        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
//            new AlertDialog.Builder(this)
//                    .setTitle("Camera")
//                    .setMessage("Please allow camera permission first")
//                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            ActivityCompat.requestPermissions(ProfilePageActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST_CODE);
//                        }
//                    })
//                    .create()
//                    .show();
//
//        } else {
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST_CODE);
//        }
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
            } else {
                startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.fromParts("package", getPackageName(), null)));
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (data.getExtras() != null) {
                bitmap = (Bitmap) data.getExtras().get("data");
                profile_images.setImageBitmap(bitmap);
                Uri tempUri = getImageUri(getApplicationContext(), bitmap);
//              Uri tempUri = data.getData();
//                CALL THIS METHOD TO GET THE ACTUAL PATH
                file = getRealPathFromURI(tempUri);
            } else {
                return;
            }
        }
        if (requestCode == RESULT_GALLERY_IMAGE) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    profile_images.setImageBitmap(bitmap);
                    file = getRealPathFromURI(contentURI);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    private void requestMultiplePermissions() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            selectOption();
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            //openSettingsDialog();
                            startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", getPackageName(), null)));
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).withErrorListener(new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {
                Toast.makeText(getApplicationContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
            }
        })
                .onSameThread()
                .check();
    }
}
