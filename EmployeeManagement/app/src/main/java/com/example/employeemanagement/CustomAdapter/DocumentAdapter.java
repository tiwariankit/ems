package com.example.employeemanagement.CustomAdapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.employeemanagement.POJO.old_document_api.Datum;
import com.example.employeemanagement.R;
import java.util.List;

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.MyDocumentHolder> {
    List<Datum> oldDocuments;
    Context context;
    String dateFormat;

    public DocumentAdapter(List<Datum> oldDocuments, Context context) {
        this.oldDocuments = oldDocuments;
        this.context = context;
    }

    @NonNull
    @Override
    public MyDocumentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.olddocumentdata, parent, false);
        return new MyDocumentHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull MyDocumentHolder holder, int position) {
        holder.document_type.setText(oldDocuments.get(position).getDocumentType());
        dateFormat = oldDocuments.get(position).getDate();
        holder.document_date.setText(dateFormat.substring(0, 10));
       // holder.document_status.setText(oldDocuments.get(position).getStatus());

        if (oldDocuments.get(position).getStatus().equals("1")) {
            holder.document_status.setText("Approve");
            holder.document_status.setTextColor(Color.GREEN);
        } else if (oldDocuments.get(position).getStatus().equals("0")){
            holder.document_status.setText("Pending");
            holder.document_status.setTextColor(Color.parseColor("#F4511E"));
        }else if(oldDocuments.get(position).getStatus().equals("2")){
            holder.document_status.setText("Reject");
            holder.document_status.setTextColor(Color.parseColor("#F4511E"));
        }
    }

    @Override
    public int getItemCount() {
        return oldDocuments.size();
    }

    public class MyDocumentHolder extends RecyclerView.ViewHolder {

        TextView document_type, document_date, document_status;

        public MyDocumentHolder(@NonNull View itemView) {
            super(itemView);
            document_type = itemView.findViewById(R.id.document_type);
            document_date = itemView.findViewById(R.id.document_date);
            document_status = itemView.findViewById(R.id.document_status);
        }
    }
}
