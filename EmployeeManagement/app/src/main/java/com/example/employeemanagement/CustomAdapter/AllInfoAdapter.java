package com.example.employeemanagement.CustomAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.employeemanagement.Activity.ShowLeaveCountActivity;
import com.example.employeemanagement.ModelClass.AllInfo;
import com.example.employeemanagement.R;

import java.util.ArrayList;
import java.util.List;

public class AllInfoAdapter extends RecyclerView.Adapter<AllInfoAdapter.CustomViewHolder> {
    List<AllInfo> allInfoList;
    Context context;

    public AllInfoAdapter(List<AllInfo> allInfoList) {
        this.allInfoList = allInfoList;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.allinfoitem, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.image.setImageResource(allInfoList.get(position).getImage());
        holder.title.setText(allInfoList.get(position).getTitle());
        holder.total.setText(allInfoList.get(position).getTotal());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context = holder.itemView.getContext();
                switch (position) {
                    case 0:
                        context.startActivity(new Intent(context, ShowLeaveCountActivity.class));
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return allInfoList.size();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView title, total;
        ImageView image;

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title_info);
            total = itemView.findViewById(R.id.total_info);
            image = itemView.findViewById(R.id.imageView);
        }
    }
}