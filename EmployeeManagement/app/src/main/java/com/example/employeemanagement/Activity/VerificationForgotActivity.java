package com.example.employeemanagement.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.employeemanagement.R;

public class VerificationForgotActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView titleTextView;
    private Button otp_verify_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_forgot);
        otp_verify_btn=findViewById(R.id.otp_verify_btn);
        otpVerifyButton();
        getToolBar();
    }

    private void getToolBar(){
        toolbar = findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);
        titleTextView=findViewById(R.id.title_text);
        titleTextView.setText("Verification");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(titleTextView.getText().toString());
    }

    private void otpVerifyButton() {
        otp_verify_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(VerificationForgotActivity.this,NewPasswordActivity.class));
            }
        });
    }

}
