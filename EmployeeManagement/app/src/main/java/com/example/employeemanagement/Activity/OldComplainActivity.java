package com.example.employeemanagement.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.employeemanagement.CustomAdapter.OldComplainAdapter;
import com.example.employeemanagement.POJO.old_complain_api.Datum;
import com.example.employeemanagement.POJO.old_complain_api.GetOldComplain;
import com.example.employeemanagement.R;
import com.example.employeemanagement.Retrofit.ApiInteface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OldComplainActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView titleTextView;
    RecyclerView recyclerView;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    int api_emp_id;
    List<GetOldComplain> getOldComplainList;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old_complain);
        getOldComplainList = new ArrayList<>();
        progressDialog = new ProgressDialog(this);
        getFindViewById();
        getToolBar();
        sharedPreferences = getSharedPreferences("LoginData", 0);
        editor=sharedPreferences.edit();
        api_emp_id = sharedPreferences.getInt("api_emp_id", 0);
        getRetrofitData();
    }


    private void getFindViewById() {
        recyclerView = findViewById(R.id.oldCompllainRecycler);
    }

    private void getToolBar() {
        toolbar = findViewById(R.id.toolbars);
        titleTextView = findViewById(R.id.title_text);
        titleTextView.setText("Old Complains");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(titleTextView.getText().toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_complain, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_complain:
                startActivity(new Intent(getApplicationContext(), ComplainActivity.class));
//                progressDialog.dismiss();
        }
        return super.onOptionsItemSelected(item);
    }

    private void getRetrofitData() {
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetOldComplain> getOldComplainCall = apiInteface.getOldComplain(api_emp_id);
        getOldComplainCall.enqueue(new Callback<GetOldComplain>() {
            @Override
            public void onResponse(Call<GetOldComplain> call, Response<GetOldComplain> response) {
                // Log.d("res", response.body().getStatus().toString());
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == true) {
                        List<Datum> data = response.body().getData();
                        progressDialog.dismiss();
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        OldComplainAdapter oldComplainAdapter = new OldComplainAdapter(data, getApplicationContext());
                        recyclerView.setAdapter(oldComplainAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetOldComplain> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(OldComplainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshPage();
    }

    private void refreshPage() {
        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetOldComplain> getOldComplainCall = apiInteface.getOldComplain(api_emp_id);
        getOldComplainCall.enqueue(new Callback<GetOldComplain>() {
            @Override
            public void onResponse(Call<GetOldComplain> call, Response<GetOldComplain> response) {
                // Log.d("res", response.body().getStatus().toString());
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == true) {
                        List<Datum> data = response.body().getData();
                        progressDialog.dismiss();
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        OldComplainAdapter oldComplainAdapter = new OldComplainAdapter(data, getApplicationContext());
                        recyclerView.setAdapter(oldComplainAdapter);
                    }
                }
            }
            @Override
            public void onFailure(Call<GetOldComplain> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(OldComplainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
