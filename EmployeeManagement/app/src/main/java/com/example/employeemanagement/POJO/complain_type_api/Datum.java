
package com.example.employeemanagement.POJO.complain_type_api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("complain_type_id")
    @Expose
    private String complainTypeId;
    @SerializedName("complain_type")
    @Expose
    private String complainType;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public String getComplainTypeId() {
        return complainTypeId;
    }

    public void setComplainTypeId(String complainTypeId) {
        this.complainTypeId = complainTypeId;
    }

    public String getComplainType() {
        return complainType;
    }

    public void setComplainType(String complainType) {
        this.complainType = complainType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
