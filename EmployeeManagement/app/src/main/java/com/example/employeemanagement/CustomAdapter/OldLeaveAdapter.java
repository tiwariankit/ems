package com.example.employeemanagement.CustomAdapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.employeemanagement.Activity.OldLeaveActivity;
import com.example.employeemanagement.POJO.leave_type_api.CancelLeave;
import com.example.employeemanagement.POJO.old_leave_api.Datum;
import com.example.employeemanagement.R;
import com.example.employeemanagement.Retrofit.ApiInteface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OldLeaveAdapter extends RecyclerView.Adapter<OldLeaveAdapter.MyOldLeaveHolder> implements Filterable {
    List<Datum> oldLeave;
    List<Datum> filterLeave;
    Context context;
    Dialog dialog;
    String from_day_leave;
    String to_day_leave;
    Date fromdate, todate;
    int leave_id, api_emp_id;
    EditText reason_editext;
    SharedPreferences sharedPreferences;
    //    ProgressDialog progressDialog=new ProgressDialog(context);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Boolean status;

    public OldLeaveAdapter(List<Datum> oldLeave, Context context) {
        this.oldLeave = oldLeave;
        this.context = context;
        this.filterLeave = new ArrayList<>(oldLeave);
    }

    @NonNull
    @Override
    public MyOldLeaveHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.oldleavedata, parent, false);
        return new MyOldLeaveHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final MyOldLeaveHolder holder, int position) {
        leave_id = oldLeave.get(position).getLeave_id();
        holder.leave_style.setText(oldLeave.get(position).getLeaveType());
        holder.leave_date.setText(oldLeave.get(position).getFromDate());
        // holder.leave_status.setText(oldLeave.get(position).getStatus());
        //holder.leave_description.setText(oldLeave.get(position).getDescription());


        from_day_leave = oldLeave.get(position).getFromDate();
        to_day_leave = oldLeave.get(position).getToDate();
        try {
            fromdate = simpleDateFormat.parse(from_day_leave);
            todate = simpleDateFormat.parse(to_day_leave);
//            totaldays=fromdate-todate;

            Log.d("dd", String.valueOf(fromdate.getTime()));
            Log.d("aa", String.valueOf(todate.getTime()));
            Log.d("to", String.valueOf(Math.abs((fromdate.getTime() - todate.getTime()) / (24 * 60 * 60 * 1000)) + 1));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d("date", from_day_leave);
        Log.d("date", to_day_leave);
        holder.leave_days.setText("Leave days:" + String.valueOf(Math.abs((fromdate.getTime() - todate.getTime()) / (24 * 60 * 60 * 1000)) + 1));
        if (oldLeave.get(position).getStatus().equals("1")) {
            holder.leave_status.setText("Approved");
            holder.leave_status.setTextColor(Color.GREEN);
            holder.cancel_btn.setEnabled(false);
            holder.cancel_btn.setBackgroundColor(Color.parseColor("#D3D3D3"));
        } else if (oldLeave.get(position).getStatus().equals("0")) {
            holder.leave_status.setText("Pending");
            holder.cancel_btn.setEnabled(true);
            holder.cancel_btn.setBackgroundColor(ContextCompat.getColor(context,R.color.Resend_password_text_color));
            holder.leave_status.setTextColor(Color.parseColor("#F4511E"));
        } else if (oldLeave.get(position).getStatus().equals("2")) {
            holder.leave_status.setText("Rejected");
            holder.leave_status.setTextColor(Color.RED);
            holder.cancel_btn.setEnabled(false);
            holder.cancel_btn.setBackgroundColor(Color.parseColor("#D3D3D3"));
        } else if (oldLeave.get(position).getStatus().equals("3")) {
            holder.leave_status.setText("Cancelled");
            holder.leave_status.setTextColor(Color.RED);
            holder.cancel_btn.setEnabled(false);
            holder.cancel_btn.setBackgroundColor(Color.parseColor("#D3D3D3"));
        }


        holder.cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context = holder.itemView.getContext();
                dialog = new Dialog(context);
                dialog.setContentView(R.layout.alert_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
                Button ok_alert_dailog = dialog.findViewById(R.id.ok_alert_dialog);
                reason_editext = dialog.findViewById(R.id.ed_reason);
                ok_alert_dailog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (reason_editext.getText().toString().trim().length() == 0) {
                            Toast.makeText(context, "Please give the reason", Toast.LENGTH_SHORT).show();
                        } else {
                            //Toast.makeText(context, "yes", Toast.LENGTH_SHORT).show();
                            Log.d("ed", reason_editext.getText().toString());
                            cancelLeave(position, reason_editext.getText().toString());
                            context.startActivity(new Intent(context, OldLeaveActivity.class));
                            new Intent().setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            dialog.dismiss();
                        }
                    }
                });

                TextView close_alert = dialog.findViewById(R.id.close_alert);
                close_alert.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return oldLeave.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Datum> datumList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                datumList.addAll(filterLeave);
            } else {
                String pattern = constraint.toString().toLowerCase().trim();
                for (Datum datnum : filterLeave) {
                    if (datnum.getLeaveType().toLowerCase().contains(pattern) || datnum.getFromDate().substring(8, 9).contains(pattern)) {
                        datumList.add(datnum);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = datumList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            oldLeave.clear();
            oldLeave.addAll((List) results.values);
            notifyDataSetChanged();

        }
    };

    public class MyOldLeaveHolder extends RecyclerView.ViewHolder {
        TextView leave_style, leave_date, leave_days, leave_status, leave_description;
        Button cancel_btn;

        public MyOldLeaveHolder(@NonNull View itemView) {
            super(itemView);
            leave_style = itemView.findViewById(R.id.leave_style);
            leave_date = itemView.findViewById(R.id.leave_date);
            leave_days = itemView.findViewById(R.id.leave_days);
            leave_status = itemView.findViewById(R.id.leave_status);
            cancel_btn = itemView.findViewById(R.id.cancel_btn);
            // leave_description = itemView.findViewById(R.id.leave_description);

        }
    }

    private void cancelLeave(int position, String reason) {
        if (reason_editext.getText().toString().trim().length() == 0) {
            Toast.makeText(context, "Please give the reason", Toast.LENGTH_SHORT).show();
        } else {
//            progressDialog.setCancelable(false);
//            progressDialog.setMessage("Please wait...");
//            progressDialog.show();
            sharedPreferences = context.getSharedPreferences("LoginData", 0);
            api_emp_id = sharedPreferences.getInt("api_emp_id", 0);
            ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
            Call<CancelLeave> cancelLeaveCall = apiInteface.cancelLeave(api_emp_id, oldLeave.get(position).getLeave_id(), reason, "3");
            cancelLeaveCall.enqueue(new Callback<CancelLeave>() {
                @Override
                public void onResponse(Call<CancelLeave> call, Response<CancelLeave> response) {
                    if (response.body().getStatus() == true) {
                        //  progressDialog.dismiss();
                        Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<CancelLeave> call, Throwable t) {
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}
