package com.example.employeemanagement.ModelClass;

public class EventModel {
    int event_image;
    String event_name;

    public EventModel(int event_image, String event_name) {
        this.event_image = event_image;
        this.event_name = event_name;
    }

    public int getEvent_image() {
        return event_image;
    }

    public String getEvent_name() {
        return event_name;
    }
}

