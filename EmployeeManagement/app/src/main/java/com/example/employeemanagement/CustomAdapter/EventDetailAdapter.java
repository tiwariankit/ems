package com.example.employeemanagement.CustomAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.example.employeemanagement.ModelClass.EventDetailModel;
import com.example.employeemanagement.R;

import java.util.ArrayList;
import java.util.List;

public class EventDetailAdapter extends PagerAdapter {
    List<String> eventDetail;
    Context context;
    LayoutInflater layoutInflater;

    public EventDetailAdapter(List<String> eventDetail, Context context) {
        this.eventDetail = eventDetail;
        this.context = context;
    }

    @Override
    public int getCount() {
        return eventDetail.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;
    }
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater=LayoutInflater.from(context);
        ViewGroup view= (ViewGroup) layoutInflater.inflate(R.layout.event_detail_data,container,false);
        ImageView imageView=view.findViewById(R.id.event_detail_image);
        Glide.with(context).load("https://intern.fmv.cc/EmployeeAdmin/assets/uploads/"+eventDetail.get(position)).into(imageView);
       // imageView.setImageResource(Integer.parseInt("https://intern.fmv.cc/EmployeeAdmin/assets/uploads/"+eventDetail.get(position).getImage()));
        container.addView(view);
        return view;
    }
}
