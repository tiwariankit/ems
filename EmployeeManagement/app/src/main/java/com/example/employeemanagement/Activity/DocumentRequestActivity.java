package com.example.employeemanagement.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.employeemanagement.CustomAdapter.DocumentAdapter;
import com.example.employeemanagement.POJO.document_type_api.GetDocuments;
import com.example.employeemanagement.POJO.old_document_api.Datum;
import com.example.employeemanagement.POJO.old_document_api.GetOldDocument;
import com.example.employeemanagement.R;
import com.example.employeemanagement.Retrofit.ApiInteface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DocumentRequestActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView titleTextView,demotxt;
    RecyclerView recyclerView;
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    int api_emp_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_request);
        sharedPreferences = getSharedPreferences("LoginData", 0);
        editor=sharedPreferences.edit();
        api_emp_id = sharedPreferences.getInt("api_emp_id", 0);
        progressDialog = new ProgressDialog(this);
        getFindViewById();
        getToolBar();
        getRetrofitData();
    }

    private void getFindViewById() {
        recyclerView = findViewById(R.id.oldDocumentRecycler);
    }

    private void getToolBar() {
        toolbar = findViewById(R.id.toolbars);
        titleTextView = findViewById(R.id.title_text);
        titleTextView.setText("Documents");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(titleTextView.getText().toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.leave, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_leave:
                startActivity(new Intent(DocumentRequestActivity.this, DocumentActivity.class));
        }
        return super.onOptionsItemSelected(item);

    }

    private void getRetrofitData() {

        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetOldDocument> getOldDocumentCall = apiInteface.getOldDocument(api_emp_id);
        getOldDocumentCall.enqueue(new Callback<GetOldDocument>() {
            @Override
            public void onResponse(Call<GetOldDocument> call, Response<GetOldDocument> response) {
//                Log.d("res",response.body().getStatus().toString());
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == true) {
                        List<Datum> data = response.body().getData();
                        editor.putInt("allDocument",data.size());
                        editor.apply();
                        progressDialog.dismiss();
                        DocumentAdapter documentAdapter = new DocumentAdapter(data, getApplicationContext());
                        recyclerView.setAdapter(documentAdapter);
                    }else {
                        progressDialog.dismiss();
                        Toast.makeText(DocumentRequestActivity.this, "You have not requested any document", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetOldDocument> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(DocumentRequestActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
