package com.example.employeemanagement.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.employeemanagement.CustomAdapter.AllInfoAdapter;
import com.example.employeemanagement.CustomAdapter.DocumentAdapter;
import com.example.employeemanagement.CustomAdapter.EventAdapter;
import com.example.employeemanagement.CustomAdapter.HolidayAdapter;
import com.example.employeemanagement.CustomAdapter.OldComplainAdapter;
import com.example.employeemanagement.CustomAdapter.OldLeaveAdapter;
import com.example.employeemanagement.ModelClass.AllInfo;
import com.example.employeemanagement.POJO.event_api.GetEventList;
import com.example.employeemanagement.POJO.holiday_api.GetHoliday;
import com.example.employeemanagement.POJO.old_complain_api.GetOldComplain;
import com.example.employeemanagement.POJO.old_document_api.GetOldDocument;
import com.example.employeemanagement.POJO.old_leave_api.Datum;
import com.example.employeemanagement.POJO.old_leave_api.GetOldLeave;
import com.example.employeemanagement.R;
import com.example.employeemanagement.Retrofit.ApiInteface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllInfoActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    List<AllInfo> allInfoList = new ArrayList<>();
    int api_emp_id;
    String allLeave;
    String allComplain;
    String allHoliday;
    String allEvent;
    String allDocument;
    RecyclerView allInfoRecyclerView;
    Toolbar toolbar;
    TextView titletextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_info);
        progressDialog = new ProgressDialog(this);
        allInfoRecyclerView = findViewById(R.id.allInfoRecycler);
        sharedPreferences = getSharedPreferences("LoginData", 0);
        api_emp_id = sharedPreferences.getInt("api_emp_id", 0);
        getToolBar();
        getLevave();
        getComplain();
        getHoliday();
        getEvent();
        getDocument();
        getRecylerData();

    }

    private void getRecylerData() {
        AllInfoAdapter allInfoAdapter = new AllInfoAdapter(allInfoList);
        allInfoRecyclerView.setAdapter(allInfoAdapter);
        allInfoRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    private void getDocument() {
        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetOldDocument> getOldDocumentCall = apiInteface.getOldDocument(api_emp_id);
        getOldDocumentCall.enqueue(new Callback<GetOldDocument>() {
            @Override
            public void onResponse(Call<GetOldDocument> call, Response<GetOldDocument> response) {
//                Log.d("res",response.body().getStatus().toString());
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == true) {
                        allDocument = String.valueOf(response.body().getData().size());
                        allInfoList.add(new AllInfo("Document", allDocument, R.drawable.document));
                        getRecylerData();
                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetOldDocument> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void getEvent() {

        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetEventList> getEventListCall = apiInteface.getEvent();
        getEventListCall.enqueue(new Callback<GetEventList>() {
            @Override
            public void onResponse(Call<GetEventList> call, Response<GetEventList> response) {
                if (response.isSuccessful()) {
                    if (response.body().status == false) {
                        progressDialog.dismiss();
                    } else {
                        allEvent = String.valueOf(response.body().data.size());
                        allInfoList.add(new AllInfo("Event", allEvent, R.drawable.event));
                        getRecylerData();
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetEventList> call, Throwable t) {
            }
        });
    }

    private void getHoliday() {
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetHoliday> getHolidayCall = apiInteface.getHolidayList();
        getHolidayCall.enqueue(new Callback<GetHoliday>() {
            @Override
            public void onResponse(Call<GetHoliday> call, Response<GetHoliday> response) {
//                Log.d("res",response.body().getStatus().toString());
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == true) {
                        allHoliday = String.valueOf(response.body().getData().size());
                        allInfoList.add(new AllInfo("Holiday", allHoliday, R.drawable.holiday));
                        getRecylerData();
                        progressDialog.dismiss();

                    }
                }
            }

            @Override
            public void onFailure(Call<GetHoliday> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void getComplain() {
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetOldComplain> getOldComplainCall = apiInteface.getOldComplain(api_emp_id);
        getOldComplainCall.enqueue(new Callback<GetOldComplain>() {
            @Override
            public void onResponse(Call<GetOldComplain> call, Response<GetOldComplain> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == true) {
                        allComplain = String.valueOf(response.body().getData().size());
                        allInfoList.add(new AllInfo("Complain", allComplain, R.drawable.complain));
                        getRecylerData();
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetOldComplain> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }


    private void getLevave() {
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetOldLeave> getOldLeaveCall = apiInteface.getOldLeave(api_emp_id);
        getOldLeaveCall.enqueue(new Callback<GetOldLeave>() {
            @Override
            public void onResponse(Call<GetOldLeave> call, Response<GetOldLeave> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == true) {
                        List<Datum> data = response.body().getData();
                        allLeave = String.valueOf(data.size());
                        progressDialog.dismiss();
                        allInfoList.add(0,new AllInfo("Leave", allLeave, R.drawable.leave));
                        getRecylerData();
                    } else {
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetOldLeave> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }

    private void getToolBar() {
        toolbar = findViewById(R.id.toolbars);
        titletextView = findViewById(R.id.title_text);
        titletextView.setText("All Information");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(titletextView.getText().toString());
    }
}
