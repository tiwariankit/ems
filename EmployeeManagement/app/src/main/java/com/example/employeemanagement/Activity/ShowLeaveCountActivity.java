package com.example.employeemanagement.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.employeemanagement.POJO.old_leave_api.GetOldLeave;
import com.example.employeemanagement.R;
import com.example.employeemanagement.Retrofit.ApiInteface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowLeaveCountActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    int api_emp_id;
    TextView textView1, textView2, textView3, textView4, textView5, textView6;
    ProgressDialog progressDialog;
    Toolbar toolbar;
    TextView titletextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_leave_count);
        getToolBar();
        progressDialog = new ProgressDialog(this);
        textView1 = findViewById(R.id.tvCasualTotal);
        textView2 = findViewById(R.id.tvAdverseweatherTotal);
        textView3 = findViewById(R.id.tvPersonalleaveTotal);
        textView4 = findViewById(R.id.tvPaternityleaveTotal);
        textView5 = findViewById(R.id.MaternityLeaveTotal);
        textView6 = findViewById(R.id.tvOthersTotal);
        sharedPreferences = getSharedPreferences("LoginData", 0);
        api_emp_id = sharedPreferences.getInt("api_emp_id", 0);
        countLeaveCategoryWise();
    }

    private void getToolBar() {
        toolbar = findViewById(R.id.toolbars);
        titletextView = findViewById(R.id.title_text);
        titletextView.setText("Total Leave");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(titletextView.getText().toString());
    }

    private void countLeaveCategoryWise() {
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        ApiInteface apiInteface = ApiInteface.retrofit.create(ApiInteface.class);
        Call<GetOldLeave> getOldLeaveCall = apiInteface.getOldLeave(api_emp_id);
        getOldLeaveCall.enqueue(new Callback<GetOldLeave>() {
            @Override
            public void onResponse(Call<GetOldLeave> call, Response<GetOldLeave> response) {
                //Log.d("res",response.body().getStatus().toString());
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        progressDialog.dismiss();
                        int casualTotal = 0;
                        int adverseWeatherTotal = 0;
                        int personalLeaveTotal = 0;
                        int maternityLeaveTotal = 0, paternityLeaveTotal = 0, othersTotal = 0, followTotal = 0;
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            if (response.body().getData().get(i).getStatus().equals("1")) {
//                                Log.d("i", response.body().getData().get(i).getLeaveType());
                                if (response.body().getData().get(i).getLeaveType().trim().equals("casual")) {
                                    casualTotal += 1;
                                    Log.d("casual", String.valueOf(casualTotal));
                                } else if (response.body().getData().get(i).getLeaveType().equals("Personal leave")) {
                                    personalLeaveTotal += 1;
                                    Log.d("personal", String.valueOf(personalLeaveTotal));
                                } else if (response.body().getData().get(i).getLeaveType().equals("Adverse weather")) {
                                    adverseWeatherTotal += 1;
                                } else if (response.body().getData().get(i).getLeaveType().equals("Maternity Leave")) {
                                    maternityLeaveTotal += 1;
                                } else if (response.body().getData().get(i).getLeaveType().equals("Paternity leave")) {
                                    paternityLeaveTotal += 1;
                                } else if (response.body().getData().get(i).getLeaveType().equals("Others")) {
                                    othersTotal += 1;
                                }
                            }
                        }
                        textView1.setVisibility(View.VISIBLE);
                        textView1.setText("" + casualTotal);
                        textView2.setText("" + adverseWeatherTotal);
                        textView3.setText("" + personalLeaveTotal);
                        textView4.setText("" + paternityLeaveTotal);
                        textView5.setText("" + maternityLeaveTotal);
                        textView6.setText("" + othersTotal);
                        Log.d("casual", String.valueOf(adverseWeatherTotal));
                        Log.d("casual", String.valueOf(maternityLeaveTotal));
                        Log.d("casual", String.valueOf(paternityLeaveTotal));
                        Log.d("casual", String.valueOf(othersTotal));
                        Log.d("casual", String.valueOf(followTotal));
                    }

                }
            }

            @Override
            public void onFailure(Call<GetOldLeave> call, Throwable t) {
                progressDialog.dismiss();
//                Toast.makeText(OldLeaveActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

}
