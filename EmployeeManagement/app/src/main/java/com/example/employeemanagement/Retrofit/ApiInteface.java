package com.example.employeemanagement.Retrofit;

import com.example.employeemanagement.POJO.ProfileUpdate;
import com.example.employeemanagement.POJO.complain_type_api.GetComplain;
import com.example.employeemanagement.POJO.complain_type_api.InsertComplain;
import com.example.employeemanagement.POJO.document_type_api.GetDocuments;
import com.example.employeemanagement.POJO.document_type_api.InsertDocument;
import com.example.employeemanagement.POJO.event_api.GetEventList;
import com.example.employeemanagement.POJO.holiday_api.GetHoliday;
import com.example.employeemanagement.POJO.leave_type_api.CancelLeave;
import com.example.employeemanagement.POJO.leave_type_api.InsertLeave;
import com.example.employeemanagement.POJO.leave_type_api.GetLeaveType;
import com.example.employeemanagement.POJO.login_api.Login;
import com.example.employeemanagement.POJO.old_complain_api.GetOldComplain;
import com.example.employeemanagement.POJO.old_document_api.GetOldDocument;
import com.example.employeemanagement.POJO.old_leave_api.GetOldLeave;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface ApiInteface {

    String BASE_URL = "https://intern.fmv.cc/EmployeeAdmin/";
    String FEED_URL = "employee-login";

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @FormUrlEncoded
    @POST(FEED_URL)
    Call<Login> getLogin(
            @Field("email") String email,
            @Field("password") String password
    );

    @GET("employee-leavetype")
    Call<GetLeaveType> getLevaveType();

    @GET("employee-documenttype")
    Call<GetDocuments> getDocumentType();

    @GET("employee-complaintype")
    Call<GetComplain> getComplainType();

    @GET("employee-event")
    Call<GetEventList> getEvent();

    @GET("employee-holiday")
    Call<GetHoliday> getHolidayList();

    @FormUrlEncoded
    @POST("employee-insertleave")
    Call<InsertLeave> insertLeave(
            @Field("employee_id") int employee_id,
            @Field("leave_type_id") int leave_type_id,
            @Field("description") String description,
            @Field("from_date") String from_date,
            @Field("to_date") String to_date
    );

    @FormUrlEncoded
    @POST("employee-cancelleave")
    Call<CancelLeave> cancelLeave(
            @Field("employee_id") int employee_id,
            @Field("leave_id") int leave_id,
            @Field("reason") String reason,
            @Field("status") String status
    );

    @FormUrlEncoded
    @POST("employee-insertcomplain")
    Call<InsertComplain> insertComplain(
            @Field("employee_id") int employee_id,
            @Field("complain_type_id") int complain_type_id,
            @Field("description") String description
    );

    @FormUrlEncoded
    @POST("employee-complainlist")
    Call<GetOldComplain> getOldComplain(
            @Field("id") int employee_id
    );

    @FormUrlEncoded
    @POST("employee-leavelist")
    Call<GetOldLeave> getOldLeave(
            @Field("id") int employee_id
    );

    @FormUrlEncoded
    @POST("employee-documentrequest")
    Call<InsertDocument> insertDocument(
            @Field("employee_id") int employee_id,
            @Field("document_type_id") int document_type_id
    );

    @FormUrlEncoded
    @POST("employee-documentlist")
    Call<GetOldDocument> getOldDocument(
            @Field("id") int employee_id
    );

    @Multipart
    @POST("employee-updateemployee")
    Call<ProfileUpdate> getProfileUpdate(
            @Part("employee_id") int emp_id,
            @Part("contact") String contact_no,
            @Part MultipartBody.Part files
    );

}
